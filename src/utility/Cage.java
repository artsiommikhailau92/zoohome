package utility;

import animals.Animal;

public class Cage {
    private int size = 10;
    private Animal[] animals;

    public Cage() {
        animals = new Animal[size];
    }

    public Cage(int size) {
        this.size = size;
        //TODO call empty constructor
    }

    public boolean addAnimal(Animal animal) {
        for (int i = 0; i < animals.length; i++) {
            if (animals[i] == null) {
                animals[i] = animal;
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        String result = "Info: ";
        for (int i = 0;i< animals.length;i++){
            if (animals[i] !=null){
                result += animals[i].toString() + "; ";
            }
        }
        if (result.equals("Info: ")){
            result = "No animals.";
        }
        return result;
    }
}

