package animals;

public class Wolf extends Predator {
    private int quantityRedCapsEaten;

    public Wolf(String nickName, double size) {
        super(nickName, size);
        type = "wolf";
    }

    @Override
    public void sound() {
        System.out.println("woof woof");
    }
}
