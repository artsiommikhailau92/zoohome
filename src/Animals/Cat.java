package animals;

public class Cat extends Predator {

    public Cat(String nickName, double size) {

        super(nickName, size);
        type = "Cat";
        bloodTemperature = 41;
    }

    @Override
    public void sound() {
        System.out.println("myau myau");
    }

    public String toString() {
        return (getNickName() + " " + getSize() + " " + type);
    }

}

