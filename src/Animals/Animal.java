package animals;

public abstract class Animal {
    private String nickName;
    private double size;
    static String type;

    public Animal(String nickName, double size) {
        this.nickName = nickName;
        this.size = size;
    }

    public abstract void sound();

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public double getSize() {
        return size;
    }

    @Override
    public String toString() {
        return (this.nickName + " " + this.size);
    }
}
