package animals;

public class Rabbit extends Herbivore {
    public Rabbit(String nickName, double size) {
        super(nickName, size);
    }

    @Override
    public void sound() {
        System.out.println("fyr fyr");
    }
}
