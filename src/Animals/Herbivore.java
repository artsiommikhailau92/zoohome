package animals;

public abstract class Herbivore extends Mammal{
    boolean isRuminant;
    public Herbivore(String nickName, double size) {
        super(nickName, size);
    }
}
