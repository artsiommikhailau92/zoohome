import animals.*;
import utility.Cage;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Cage cage = new Cage();
        Wolf wolf = new Wolf("Petya", 14.5);
        cage.addAnimal(wolf);
        cage.addAnimal(new Rabbit("Carrot", 4.5));
        cage.addAnimal(new Bird("Kesha", 7.3));
        cage.addAnimal(new Wolf("Ibiza", 16));
        cage.addAnimal(new Cat("Martin", 10));
        System.out.println(cage);
    }
}

